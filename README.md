# -Huella Ecologica-
V 1.0.0

Developed by compuandsoft.

## Target Platform
```bash
.Net Core 3.1
```

## Config
### 1. Restore nuget dependencies 

```bash
dotnet restore <SOLUTION>.sln
```
### 2. Build solution

```bash
dotnet build <SOLUTION>.sln
```