﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace EcoMark.Business.ApplicationService.Abstract.Common
{
    public interface IBaseSimpleApplicationService
    {
        #region Sync
        /// <summary>
        ///     Saves all changes made in the Context to the Database
        /// </summary>
        /// <returns>The number of state entries written to the DB</returns>
        int SaveChanges();
        #endregion

        #region Async
        /// <summary>
        ///     Asynchronously saves all changes made in the Context to the Database
        /// </summary>
        /// <returns>The number of state entries written to the DB</returns>
        Task<int> SaveChangesAsync();
        #endregion
    }
}
