﻿using EcoMark.Business.ApplicationService.Abstract.Common;
using EcoMark.Domain.Model;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace EcoMark.Business.ApplicationService.Abstract
{
    public interface IAuthApplicationService : IBaseSimpleApplicationService
    {
        Task<User> AddUserAsync(User user);
    }
}
