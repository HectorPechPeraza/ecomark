﻿using EcoMark.Business.ApplicationService.Abstract;
using EcoMark.Business.ApplicationService.Abstract.Common;
using EcoMark.Business.ApplicationService.Implementation.Common;
using EcoMark.Data.Repository.Abstract;
using EcoMark.Data.Repository.Abstract.Common;
using EcoMark.Domain.Model;
using Microsoft.AspNetCore.Http;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace EcoMark.Business.ApplicationService.Implementation
{
    public class AuthApplicationService : BaseSimpleApplicationService, IAuthApplicationService
    {
        private IUserRepository _UserRepository;

        public AuthApplicationService(
            ISimpleRepository repository,
            IUserRepository userRepository,
            IHttpContextAccessor httpContextAccessor) : base(repository, httpContextAccessor)
        {
            this._UserRepository = userRepository;
        }

        public Task<User> AddUserAsync(User user)
        {
            return this._UserRepository.AddAsync(user);
        }
    }
}
