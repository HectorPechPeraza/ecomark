﻿using EcoMark.Business.ApplicationService.Abstract;
using EcoMark.Business.ApplicationService.Abstract.Common;
using EcoMark.Business.ApplicationService.Implementation;
using EcoMark.Business.ApplicationService.Implementation.Common;
using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.DependencyInjection;
using System;
using System.Collections.Generic;
using System.Text;

namespace EcoMark.Business.Extensions
{
    public static class DependencyRegistry
    {
        public static IServiceCollection ConfigureBusinessDependencies(this IServiceCollection services)
        {
            services.AddSingleton<IHttpContextAccessor, HttpContextAccessor>();
            services.AddScoped<IAuthApplicationService, AuthApplicationService>();

            return services;
        }
    }
}
