﻿using EcoMark.Domain.Model;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Text;

namespace EcoMark.Data.Contexts
{
    public class EcoMarkDbContext : DbContext
    {
        public EcoMarkDbContext(DbContextOptions<EcoMarkDbContext> options) : base(options)
        {

        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            base.OnModelCreating(modelBuilder);
        }

        // Define Database entities
        // Example => public virtual DbSet<ENTITY> ENTITY-DATABASE-NAME { get; set; }
        // Entities come from EcoMark.Domain.Model

        public virtual DbSet<User> Users { get; set; }
}
}
