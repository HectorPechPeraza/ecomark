﻿using EcoMark.Domain.Model.Identity;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Identity.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Text;

namespace EcoMark.Data.Contexts
{
    public class EcoMarkIdentityDbContext : IdentityDbContext<ApplicationUser>
    {
        public EcoMarkIdentityDbContext(DbContextOptions<EcoMarkIdentityDbContext> options) : base(options)
        { }

        protected override void OnModelCreating(ModelBuilder builder)
        {
            var roles = new List<IdentityRole>
            {
                new IdentityRole
                {
                    Name="Master",
                    NormalizedName="master"
                },
                new IdentityRole
                {
                    Name="RegularUser",
                    NormalizedName="regularUser"
                }
            };

            var masterUser = new ApplicationUser()
            {
                Name = "root",
                Email = "gerencia@compuandsoft.com",
                UserName = "gerencia@compuandsoft.com",
                EmailConfirmed = true,
                NormalizedEmail = "GERENCIA@COMPUANDSOFT.COM",
                NormalizedUserName = "GERENCIA@COMPUANDSOFT.COM",
                PasswordHash = "AQAAAAEAACcQAAAAEBjtR+urIPtSvvqeqLc3GEomFxqxtVnyeIarkV0gQ7VO4EuwLOn7JzkpHyl90hmEJw==",
                SecurityStamp = "e3e1cfe3-f6e3-4ace-a0d7-89c5a4b8e318"
            };

            builder.Entity<IdentityRole>().HasData(roles);
            builder.Entity<ApplicationUser>().HasData(masterUser);
            builder.Entity<IdentityUserRole<string>>().HasData(new IdentityUserRole<string> { RoleId = roles[0].Id, UserId = masterUser.Id });

            base.OnModelCreating(builder);
        }
    }
}
