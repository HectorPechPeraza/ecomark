﻿using EcoMark.Data.Repository.Abstract;
using EcoMark.Data.Repository.Abstract.Common;
using EcoMark.Data.Repository.Implementation;
using EcoMark.Data.Repository.Implementation.Common;
using Microsoft.Extensions.DependencyInjection;
using System;
using System.Collections.Generic;
using System.Text;

namespace EcoMark.Data.Extensions
{
    public static class DependencyRegistry
    {
        public static IServiceCollection ConfigureDataAccessDependencies(this IServiceCollection services)
        {
            services.AddScoped<IBaseDapperRepository, BaseDapperRepository>();
            services.AddScoped<ISimpleRepository, SimpleRepository>();
            services.AddScoped<IUserRepository, UserRepository>();

            return services;
        }
    }
}
