# DataAccess Implementation Readme
NOTE: Install Whack Whack Terminal to run the .Net cli commands or use another Terminal
### First Steps:

### IMPORTANT! Set console path to: 
```
[path]/src/Data
```

### Create Migrations:
```
dotnet ef -s ../Web migrations add <YourMigrationName> -c EcoMarkDbContext -o "Migrations/EcoMarkDb"
```

### Run Migrations:
```
dotnet ef -s ../Web database update -c EcoMarkDbContext
```

### Remove Migrations:
```
dotnet ef -s ../Web migrations remove -c EcoMarkDbContext
```

### [FIX] Update dotnet ef version:
```
dotnet tool install --global dotnet-ef --version 3.0.0
```