﻿using Dapper;
using System;
using System.Collections.Generic;
using System.Data;
using System.Text;
using System.Threading.Tasks;

namespace EcoMark.Data.Repository.Abstract.Common
{
    public interface IBaseDapperRepository
    {
        #region Read
        T QuerySingleOrDefault<T>(string query, DynamicParameters pars, CommandType? commandType = null);

        Task<T> QuerySingleOrDefaultAsync<T>(string query, DynamicParameters pars, CommandType? commandType = null);

        IEnumerable<T> Query<T>(string query, DynamicParameters pars, CommandType? commandType = null);

        Task<IEnumerable<T>> QueryAsync<T>(string query, DynamicParameters pars, CommandType? commandType = null);
        #endregion
    }
}
