﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace EcoMark.Data.Repository.Abstract.Common
{
    public interface IBaseSimpleRepository : IDisposable
    {
        #region Sync
        /// <summary>
        ///     Saves all changes made in the Context to the Database
        /// </summary>
        /// <returns>The number of state entries written to the DB</returns>
        int SaveChanges();
        #endregion

        #region Async
        /// <summary>
        ///     Asynchronously saves all changes made in the Context to the Database
        /// </summary>
        /// <returns>The number of state entries written to the DB</returns>
        Task<int> SaveChangesAsync();
        #endregion
    }
}
