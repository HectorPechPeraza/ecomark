﻿using EcoMark.Data.Repository.Abstract.Common;
using EcoMark.Domain.Model;
using System;
using System.Collections.Generic;
using System.Text;

namespace EcoMark.Data.Repository.Abstract
{
    public interface IUserRepository : IBaseRepository<User, int>
    {
    }
}
