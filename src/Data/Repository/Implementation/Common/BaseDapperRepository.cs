﻿using Dapper;
using EcoMark.Data.Repository.Abstract.Common;
using EcoMark.Domain.Options;
using Microsoft.Data.SqlClient;
using Microsoft.Extensions.Options;
using System;
using System.Collections.Generic;
using System.Data;
using System.Text;
using System.Threading.Tasks;

namespace EcoMark.Data.Repository.Implementation.Common
{
    public class BaseDapperRepository : IBaseDapperRepository
    {
        protected readonly string _connString;

        public BaseDapperRepository(IOptions<DatabaseOptions> options)
        {
            this._connString = options.Value.ConnectionString;
        }

        #region READ        
        public T QuerySingleOrDefault<T>(string query, DynamicParameters pars, CommandType? commandType = null)
        {
            T result;

            using (var conn = new SqlConnection(_connString))
            {
                result = conn.QuerySingleOrDefault<T>(query, pars, commandType: commandType);
            }

            return result;
        }

        public async Task<T> QuerySingleOrDefaultAsync<T>(string query, DynamicParameters pars, CommandType? commandType = null)
        {
            T result;

            using (var conn = new SqlConnection(_connString))
            {
                result = await conn.QuerySingleOrDefaultAsync<T>(query, pars, commandType: commandType);
            }

            return result;
        }

        public IEnumerable<T> Query<T>(string query, DynamicParameters pars, CommandType? commandType = null)
        {
            IEnumerable<T> result;

            using (var conn = new SqlConnection(_connString))
            {
                result = conn.Query<T>(query, pars, commandType: commandType);
            }

            return result;
        }

        public async Task<IEnumerable<T>> QueryAsync<T>(string query, DynamicParameters pars, CommandType? commandType = null)
        {
            IEnumerable<T> result;

            using (var conn = new SqlConnection(_connString))
            {
                result = await conn.QueryAsync<T>(query, pars, commandType: commandType);
            }

            return result;
        }
        #endregion
    }
}
