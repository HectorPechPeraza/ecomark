﻿using EcoMark.Data.Contexts;
using EcoMark.Data.Repository.Abstract.Common;
using EcoMark.Domain.Model.Common;
using Microsoft.Data.SqlClient;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EcoMark.Data.Repository.Implementation.Common
{
    public class BaseSimpleRepository : IBaseSimpleRepository
    {
        /// <summary>
        /// Gets the Actual DBContext
        /// </summary>
        public DbContext DbContext { get; }
        /// <summary>
        /// To detect redundant calls
        /// </summary>
        private bool disposedValue = false;

        protected BaseSimpleRepository(DbContext dbContext)
        {
            this.DbContext = dbContext;
            if (this.DbContext is EcoMarkDbContext)
            {
                // this.Entities = (this.DbContext as EcoMarkDbContext).Set<TEntity>();
            }
        }

        #region Sync
        private void UpdateSoftDeletableEntities()
        {
            var deletedEntries = this.DbContext
                                    .ChangeTracker
                                    .Entries()
                                    .Where(x => x.Entity is ISoftDeletable &&
                                                 x.State == EntityState.Deleted);
            foreach (var entry in deletedEntries)
            {
                if (entry?.Entity is ISoftDeletable entity)
                {
                    entry.State = EntityState.Modified;
                    entity.IsDeleted = true;
                }
            }
        }
        /// <summary>
        ///     Saves all changes made in the Context to the Database
        /// </summary>
        /// <returns>The number of state entries written to the DB</returns>
        public int SaveChanges()
        {
            try
            {
                this.UpdateSoftDeletableEntities();
                return this.DbContext.SaveChanges();
            }
            catch (SqlException sqlEx)
            {
                throw sqlEx;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        #endregion

        #region Async
        /// <summary>
        ///     Asynchronously saves all changes made in the Context to the Database
        /// </summary>
        /// <returns>The number of state entries written to the DB</returns>
        public async Task<int> SaveChangesAsync()
        {
            try
            {
                this.UpdateSoftDeletableEntities();
                return await this.DbContext.SaveChangesAsync();
            }
            catch (DbUpdateException dbuEx)
            {
                throw dbuEx;
            }
            catch (SqlException sqlEx)
            {
                throw sqlEx;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public void Dispose()
        {
            this.Dispose(true);
        }
        protected virtual void Dispose(bool disposing)
        {
            if (!this.disposedValue)
            {
                if (disposing)
                {
                    this.DbContext.Dispose();
                }

                // TODO: free unmanaged resources (unmanaged objects) and override a finalizer below.
                // TODO: set large fields to null.
                this.disposedValue = true;
            }
        }
        #endregion
    }
}
