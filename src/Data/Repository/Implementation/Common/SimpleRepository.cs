﻿using EcoMark.Data.Contexts;
using EcoMark.Data.Repository.Abstract.Common;
using System;
using System.Collections.Generic;
using System.Text;

namespace EcoMark.Data.Repository.Implementation.Common
{
    public class SimpleRepository : BaseSimpleRepository, ISimpleRepository
    {
        public SimpleRepository(EcoMarkDbContext context) : base(context)
        {

        }
    }
}
