﻿using EcoMark.Data.Contexts;
using EcoMark.Data.Repository.Abstract;
using EcoMark.Data.Repository.Abstract.Common;
using EcoMark.Data.Repository.Implementation.Common;
using EcoMark.Domain.Model;
using System;
using System.Collections.Generic;
using System.Text;

namespace EcoMark.Data.Repository.Implementation
{
    public class UserRepository : BaseRepository<User,int>, IUserRepository
    {
        protected IBaseDapperRepository _BaseDapperRepository;

        public UserRepository(
            IBaseDapperRepository baseDapperRepository,
            EcoMarkDbContext context) : base(context)
        {
            this._BaseDapperRepository = baseDapperRepository;
        }
    }
}
