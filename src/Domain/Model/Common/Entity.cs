﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace EcoMark.Domain.Model.Common
{
    public class Entity
    {
        [Key]
        public int Id { get; set; }
    }
}
