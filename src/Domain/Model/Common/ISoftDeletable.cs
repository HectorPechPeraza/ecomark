﻿using System;
using System.Collections.Generic;
using System.Text;

namespace EcoMark.Domain.Model.Common
{
    public interface ISoftDeletable
    {
        bool IsDeleted { get; set; }
    }
}
