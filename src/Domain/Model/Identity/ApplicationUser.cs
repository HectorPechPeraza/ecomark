﻿using Microsoft.AspNetCore.Identity;
using System.ComponentModel.DataAnnotations;

namespace EcoMark.Domain.Model.Identity
{
    public class ApplicationUser : IdentityUser
    {
        [MaxLength(256)]
        public string Name { get; set; }
    }
}
