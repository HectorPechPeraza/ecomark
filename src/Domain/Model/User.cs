﻿using EcoMark.Domain.Model.Common;
using EcoMark.Domain.Model.Identity;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace EcoMark.Domain.Model
{
    public class User : Entity
    {
        public string FirstName { get; set; }

        public string LastName { get; set; }

        public DateTime Birthday { get; set; }

        [Required]
        public string Email { get; set; }
    }
}
