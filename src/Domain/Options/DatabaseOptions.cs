﻿using System;
using System.Collections.Generic;
using System.Text;

namespace EcoMark.Domain.Options
{
    public class DatabaseOptions
    {
        public string ConnectionString { get; set; }
    }
}
