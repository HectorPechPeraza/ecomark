﻿using System;
using System.Collections.Generic;
using System.Text;

namespace EcoMark.Domain.Options
{
    public class JwtOptions
    {
        public string Issuer { get; set; }
        public string Key { get; set; }
        public string ExpireMinutes { get; set; }
    }
}
