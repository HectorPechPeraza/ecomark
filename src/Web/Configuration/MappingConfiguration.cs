﻿using AutoMapper;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace EcoMark.Web.Configuration
{
    public class MappingConfiguration : Profile
    {
        public MappingConfiguration()
        {

        }

        /// <summary>
        ///      Wrapper of CreateMap<TSource, TDestination>
        ///      performs mapping in both ways
        /// </summary>
        /// <typeparam name="TSource">Source Type</typeparam>
        /// <typeparam name="TDestination">Destination Type</typeparam>
        public void CreateBothMaps<TSource, TDestination>()
        {
            this.CreateMap<TSource, TDestination>();
            this.CreateMap<TDestination, TSource>();
        }
    }
}
