﻿using EcoMark.Business.ApplicationService.Abstract;
using EcoMark.Domain.Model.Identity;
using EcoMark.Domain.ViewModels.Auth;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace EcoMark.Web.Controllers.Auth
{
    [ApiController]
    [Route("[controller]/[action]")]
    public class AuthController : ControllerBase
    {
        private IAuthApplicationService ApplicationService;
        private readonly UserManager<ApplicationUser> _userManager;

        public AuthController(
            IAuthApplicationService applicationService,
            UserManager<ApplicationUser> userManager)
        {
            this.ApplicationService = applicationService;
            this._userManager = userManager;
        }

        [HttpPost]
        public async Task<IActionResult> Login()
        {
            string name = "Hctor";
            return new JsonResult(name);
        }

        [HttpPost]
        public async Task<IActionResult> SignIn([FromBody] SignInBaseViewModel signIn)
        {
            if (!this.ModelState.IsValid || signIn == null)
            {
                return BadRequest();
            }

            try
            {
                var user = this._userManager.Users.AsEnumerable().Where(u => u.Email.Equals(signIn.Email, StringComparison.InvariantCultureIgnoreCase)).FirstOrDefault();
                if (user == null)
                {
                    return Ok("User Added");
                }
                else
                {
                    return Ok("User Already Exist");
                }

            }
            catch (Exception ex)
            {
                return BadRequest(ex.Message);
            }
        }
    }
}
