using System;
using System.Collections.Generic;
using System.IdentityModel.Tokens.Jwt;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using EcoMark.Business.Extensions;
using EcoMark.Data.Contexts;
using EcoMark.Data.Extensions;
using EcoMark.Domain.Model.Identity;
using EcoMark.Domain.Options;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Logging;
using Microsoft.IdentityModel.Tokens;

namespace EcoMark.Web
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            services.AddControllers();

            // Get connection string from appsettings.json
            string ecoMarkDb = this.Configuration.GetConnectionString("EcoMarkSql");

            // Configure database options
            services.Configure<DatabaseOptions>(options =>
            {
                options.ConnectionString = ecoMarkDb;
            });

            // Configure JWT Options
            var jwtOptions = new JwtOptions
            {
                ExpireMinutes = this.Configuration["Jwt:ExpireMinutes"],
                Issuer = this.Configuration["Jwt:Issuer"],
                Key = this.Configuration["Jwt:Key"]
            };

            // Current Assembly where lives DBContexts
            string contextAssembly = "EcoMark.Data";
            // Add DB Context
            services.AddDbContext<EcoMarkDbContext>(options =>
            {
                options.UseSqlServer(ecoMarkDb, b => b.MigrationsAssembly(contextAssembly));
            });
            services.AddDbContext<EcoMarkIdentityDbContext>(options =>
            {
                options.UseSqlServer(ecoMarkDb, b => b.MigrationsAssembly(contextAssembly));
            });

            // Add Identity
            services.AddIdentity<ApplicationUser, IdentityRole>(options =>
           {
               options.Password.RequireDigit = false;
               options.Password.RequiredLength = 1;
               options.Password.RequireLowercase = false;
               options.Password.RequireUppercase = false;
               options.Password.RequireNonAlphanumeric = false;
           })
           .AddEntityFrameworkStores<EcoMarkIdentityDbContext>()
           .AddDefaultTokenProviders();

            services.ConfigureBusinessDependencies();
            services.ConfigureDataAccessDependencies();

            // Add Jwt Authentication
            JwtSecurityTokenHandler.DefaultInboundClaimTypeMap.Clear(); // => remove default claims
            services
                .AddAuthentication(options =>
                {
                    options.DefaultAuthenticateScheme = JwtBearerDefaults.AuthenticationScheme;
                    options.DefaultScheme = JwtBearerDefaults.AuthenticationScheme;
                    options.DefaultChallengeScheme = JwtBearerDefaults.AuthenticationScheme;

                })
                .AddJwtBearer(cfg =>
                {
                    cfg.RequireHttpsMetadata = false;
                    cfg.SaveToken = true;
                    cfg.TokenValidationParameters = new TokenValidationParameters
                    {
                        ValidateIssuer = false,
                        ValidateAudience = false,
                        ValidateLifetime = true,
                        ValidateIssuerSigningKey = false,
                        ValidIssuer = jwtOptions.Issuer,
                        ValidAudience = jwtOptions.Issuer,
                        IssuerSigningKey = new SymmetricSecurityKey(Encoding.UTF8.GetBytes(jwtOptions.Key))
                    };
                });
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IWebHostEnvironment env)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }

            app.UseRouting();

            app.UseAuthentication();
            app.UseAuthorization();

            app.UseEndpoints(endpoints =>
            {
                endpoints.MapControllerRoute(
                    name: "default",
                    pattern: "{controller}/{action=Index}/{id?}");
            });

            app.UseEndpoints(endpoints =>
            {
                endpoints.MapControllers();
            });
        }
    }
}
